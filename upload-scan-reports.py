import requests, sys

file_name = sys.argv[1]
scan_type = ''

if file_name == 'gitleaks.json':
    scan_type = 'Gitleaks Scan'
elif file_name == 'njsscan.sarif':
    scan_type = 'SARIF' # scan_types are defined by defectdojo!!
elif file_name == 'semgrep.json':
    scan_type = 'Semgrep JSON Report'

headers = {
    'Authorization': 'Token fc206f994ee68ef553afd0c47dd453f8497184c3'
}

url = 'https://demo.defectdojo.org/api/v2/import-scan'

data = {
    'active': True,
    'verified': True,
    'scan_type': scan_type,
    'minimum_severity': 'Low',
    'engagement': 19
}

files = {
    'file': open(file_name, 'rb')
}

response = requests.post(url, headers=headers, data=data, files=files)

#success codes
# 200:request was received and is being processed; 
# 201: request was successful, resource was created

if response.status_code == 201:
    print('Scan results imported successfully!')
else:
    print(f'Failed to import scan results: {response.content}')
